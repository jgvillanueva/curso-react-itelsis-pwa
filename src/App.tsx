import React, {SyntheticEvent} from 'react';
import logo from './logo.svg';
import './App.scss';
import {
  useState
} from "react";

import {Routes, Route, Link, useNavigate} from 'react-router-dom';
import {Lista} from "./Components/List.component";
import {New} from "./Components/New.component";
import {Info} from "./Components/Info.component";
import {BottomNavigation, BottomNavigationAction, Paper} from "@mui/material";
import {FormatListBulleted, PlaylistAdd, Info as InfoIcon} from "@mui/icons-material";
import {Detail} from "./Components/Detail.component";

function App(): JSX.Element {
  console.log('App version:', process.env.REACT_APP_VERSION);
  const [onLine, setOnLine] = useState(window.navigator.onLine);
  window.addEventListener('online', () => setOnLine(true));
  window.addEventListener('offline', () => setOnLine(false));

  const appPaths = ['lista', 'new', 'info'];
  const navigate = useNavigate();

  const navigatetoPage = (event: SyntheticEvent, newValue: number): void => {
      navigate(appPaths[newValue]);
  }

  return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          {
            onLine ?
                <div>
                </div>
                :
                <h1>No hay red</h1>
          }
            <ul>
                <li><Link to={appPaths[0]}>Lista</Link></li>
                <li><Link to={appPaths[1]}>New</Link></li>
                <li><Link to={appPaths[2]}>Info</Link></li>
            </ul>
        </header>

          <Routes>
              <Route path="/" element={<Lista/>} />
              <Route path={appPaths[0]} element={<Lista />} >
                  <Route path=":id" element={<Detail />} />
              </Route>
              <Route path={appPaths[1]} element={<New />} />
              <Route path={appPaths[2]} element={<Info />} />
          </Routes>

          <Paper sx={{position: "fixed", bottom: 0, left: 0, right: 0}} elevation={3}>
              <BottomNavigation
                  showLabels={true}
                  onChange={navigatetoPage}
              >
                  <BottomNavigationAction icon={<FormatListBulleted />}  label="Lista"/>
                  <BottomNavigationAction icon={<PlaylistAdd />}  label="New"/>
                  <BottomNavigationAction icon={<InfoIcon />}  label="Info"/>
              </BottomNavigation>
          </Paper>
      </div>
  );
}

export default App;

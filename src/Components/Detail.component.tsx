import {FC, ReactElement, useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import {Mensaje} from "./List.component";

type IProps = {

}
export const  Detail: FC<IProps> =({}): ReactElement => {
    const url: string = process.env.REACT_APP_API_URL? process.env.REACT_APP_API_URL : 'https://dev.contanimacion.com/api_tablon/api/mensajes';
    const mensajeInicial: Mensaje = {
        id: 0,
        asunto: '',
        mensaje: '',
    };
    const [mensaje, setMensaje] = useState(mensajeInicial);
    const {id} = useParams();
    useEffect(() => {
        console.log('Pidiendo datos');
        const getData = async (): Promise<void> => {
            return new Promise((resolve) => {
                fetch(url + '/get/' + id, {method: 'GET'})
                    .then(res => res.json())
                    .then((response) => {
                        console.log(response);
                        setMensaje(response);
                        resolve();
                    })
                    .catch(e => {
                        console.log('Error cargando datos del mensaje', e);
                    })
            })
        }
        getData()
            .catch(console.error);
    }, [url, id]);

    return (
        <div>
            <h1>Detalle</h1>
            <h2>{mensaje.asunto}</h2>
            <p>{mensaje.mensaje}</p>
        </div>
    )
}
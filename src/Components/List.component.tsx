import {FC, ReactElement, useEffect, useState} from "react";
import {ListItem, ListItemText, List, ListItemIcon} from "@mui/material";
import {AddCircle, Info} from "@mui/icons-material";
import {Link, Outlet, useParams} from "react-router-dom";

export type Mensaje = {
    id: number,
    asunto: string,
    mensaje: string,
}

type IProps = {

}
export const  Lista: FC<IProps> =({}): ReactElement => {
    const url: string = process.env.REACT_APP_API_URL? process.env.REACT_APP_API_URL : 'https://dev.contanimacion.com/api_tablon/api/mensajes';
    const [mensajes, setMensajes] = useState([]);
    const {id} = useParams();
     useEffect(() => {
        console.log('Pidiendo datos');
        const getData = async (): Promise<void> => {
            return new Promise((resolve) => {
                fetch(url, {method: 'GET'})
                    .then(res => res.json())
                    .then((response) => {
                        console.log(response);
                        setMensajes(response);
                        resolve();
                    })
                    .catch(e => {
                        console.log('Error cargando datos', e);
                    })
            })
        }
        if(!id) {
            getData()
                .catch(console.error);
        } else {
            setMensajes([]);
        }
    }, [url, id]);

    const getItems = () => {
        return(
            mensajes.map((mensaje: Mensaje) => {
                return <ListItem key={mensaje.id}>
                    <ListItemIcon>
                        <Link to={mensaje.id.toString()}>
                            <Info />
                        </Link>
                    </ListItemIcon>
                    <ListItemText
                        primary={mensaje.asunto}
                        secondary={mensaje.mensaje}
                    ></ListItemText>
                </ListItem>
            })
        )
    }

    return (
        <div className="lista">
            <h1>Lista</h1>
            <Outlet/>
            <List>
                {getItems()}
            </List>

        </div>
    )
}